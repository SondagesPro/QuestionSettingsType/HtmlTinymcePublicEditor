# HtmlTinymcePublicEditor

Allow to show an HTML editor to partcipant on text question (multiple text and array text inc;luded)

Just set Yes on _Show an HTML editor to participant_ setting in _Display_ settings part to activate the HTML editor on question.

You can update tinyMCE settings by survey template with your own twig file in `./survey/questions/htmltinymcepubliceditor`.

### Via GIT
- Go to your LimeSurvey Directory
- Clone in plugins/HtmlTinymcePublicEditor directory : `git clone https://gitlab.com/SondagesPro/QuestionSettingsType/HtmlTinymcePublicEditor.git HtmlTinymcePublicEditor`

### Via ZIP file 
- Download the zip HtmlTinymcePublicEditor-master
- Extract and rename directory to HtmlTinymcePublicEditor
- Zip the directory
- Upload via Administration GUI

## Contribute and issue

Contribution are welcome, for patch and issue : use [gitlab](https://gitlab.com/QuestionSettingsType/SondagesPro/HtmlTinymcePublicEditor).

## Home page & Copyright
- HomePage <http://www.sondages.pro/>
- Copyright © 2024 Denis Chenu <http://sondages.pro>
- Licence : GNU Affero General Public License <https://www.gnu.org/licenses/agpl-3.0.html>
- [Donate](https://support.sondages.pro/open.php?topicId=12), [Liberapay](https://liberapay.com/SondagesPro/), [OpenCollective](https://opencollective.com/sondagespro) 
