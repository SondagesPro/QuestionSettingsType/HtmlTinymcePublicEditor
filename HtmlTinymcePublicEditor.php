<?php
/**
 * Allow to show an HTML editor for text questions to particpant using TinyMCE javascript library
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2024 Denis Chenu <http://www.sondages.pro>
 * @license AGPL v3
 * @version 0.3.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class HtmlTinymcePublicEditor extends PluginBase
{

    static protected $name = 'HtmlTinymcePublicEditor';
    static protected $description = 'Allow to show an HTML editor for text questions to particpant using TinyMCE javascript library.';

    /**
    * Add function to be used in beforeQuestionRender event and to attriubute
    */
    public function init()
    {
        Yii::setPathOfAlias('HtmlTinymcePublicEditor', dirname(__FILE__));
        $this->subscribe('beforeQuestionRender','activateHtmlTinymce');
        $this->subscribe('newQuestionAttributes','addHtmlTinymceAttribute');
    }

    /**
    * Add the script when question is rendered
    * Add QID and SGQ replacement forced (because it's before this was added by core
    */
    public function activateHtmlTinymce()
    {
        $oEvent = $this->getEvent();
        $aAttributes = QuestionAttribute::model()->getQuestionAttributes($oEvent->get('qid'));
        if (empty($aAttributes['htmlTinymceEditor'])) {
            return;
        }
        $scripttwig = 'tinymce_' . $aAttributes['htmlTinymceEditor'] . '_script';
        $qid = $oEvent->get('qid');
        $this->subscribe("getPluginTwigPath", "addTwigPath");
        $renderData = array(
            'aSurveyInfo' => getSurveyInfo($oEvent->get('surveyId'), App()->getLanguage()),
            'questionAttributes' => $aAttributes,
            'HtmlTinymcePublicEditor' => [
                'selector' => "#question{$qid} textarea, #question{$qid} input[type=text]",
            ]
        );
        $script = App()->twigRenderer->renderPartial(
            "/survey/questions/htmltinymcepubliceditor/{$scripttwig}.twig",
            $renderData
        );
        $this->unsubscribe("getPluginTwigPath");
        $this->registerPackage();
        App()->getClientScript()->registerScript('htmlTinymcePublicEditor' . $qid , $script, CClientScript::POS_END);
    }

    /**
     * @see event
     * just add twig directory , disable after usage
     */
    public function addTwigPath()
    {
        $viewPath = dirname(__FILE__) . "/twig";
        $this->getEvent()->append("add", [$viewPath]);
    }

    /**
    * The attribute
    */
    public function addHtmlTinymceAttribute()
    {
        $attributes = array(
            'htmlTinymceEditor' => array(
                'name'      => 'htmlTinymceEditor',
                'types'     => 'TUMQ;OP', /* All long text + multiple short text + array text + list with comment + multiple choice with comment*/
                'category'  => App()->getConfig('versionnumber') <=3 ? $this->translate('Display') : 'Display',
                'sortorder' => 400,
                'inputtype' => 'singleselect',
                'options'   => array(
                    0 => gT('No'),
                    'simple' => gT('Simple'),
                    'full' => gT('Full'),
                ),
                'caption'   => $this->translate('Show an HTML editor to participant'),
                'default'   => '0',
                'help' => $this->translate('This activate TinyMce HTML editor on all input show to user in this question. cvalue are not updated, '),
            ),
        );

        if(method_exists($this->getEvent(),'append')) {
            $this->getEvent()->append('questionAttributes', $attributes);
        } else {
            $questionAttributes=(array)$this->event->get('questionAttributes');
            $questionAttributes=array_merge($questionAttributes,$attributes);
            $this->event->set('questionAttributes',$attributes);
        }
    }

    /**
    * Function to register the package if not exist
    */
    public function registerPackage()
    {
        $minVersion = floatval(App()->getConfig("debug")) > 1 ? "" : ".min";
        if (!Yii::app()->clientScript->hasPackage('tinymce')) {
            Yii::app()->clientScript->addPackage('tinymce', array(
                'basePath'    => 'HtmlTinymcePublicEditor.vendor.tinymce.tinymce',
                'js'          => array(
                    'tinymce' . $minVersion . '.js',
                ),
                'depends'     => array('jquery')
            ));
        }
        App()->getClientScript()->registerPackage('tinymce');
    }
    /**
    * @see parent::gT
    */
    private function translate($sToTranslate, $sEscapeMode = 'unescaped', $sLanguage = null)
    {
        if(is_callable($this, 'gT')) {
            return $this->gT($sToTranslate,$sEscapeMode,$sLanguage);
        }
        return $sToTranslate;
    } 
}
