<?php return array(
    'root' => array(
        'name' => 'sondagespro/htmltinymcepubliceditor',
        'pretty_version' => '0.1.1',
        'version' => '0.1.1.0',
        'reference' => null,
        'type' => 'limesurvey-plugin',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'sondagespro/htmltinymcepubliceditor' => array(
            'pretty_version' => '0.1.1',
            'version' => '0.1.1.0',
            'reference' => null,
            'type' => 'limesurvey-plugin',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'tinymce/tinymce' => array(
            'pretty_version' => '7.0.1',
            'version' => '7.0.1.0',
            'reference' => '863759766e2397d1f639c63d006680a9e8ba6233',
            'type' => 'component',
            'install_path' => __DIR__ . '/../tinymce/tinymce',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
